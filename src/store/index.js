import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import state from './state'

Vue.use(Vuex)

export default new Vuex.Store({
  ...state,
  plugins: [ createPersistedState() ],
  strict: process.env.NODE_ENV === 'development'
})
